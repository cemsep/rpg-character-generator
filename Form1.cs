﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using RPGCharacterGeneratorLogic;
using Logic;
using Microsoft.Data.SqlClient;

namespace RPGCharacterGenerator
{
    public partial class Form1 : Form
    {
        SqlConnectionStringBuilder builder;

        public Form1()
        {
            InitializeComponent();
            raceComboBox.DataSource = Enum.GetValues(typeof(RaceEnum));
            classComboBox.DataSource = Enum.GetValues(typeof(ClassEnum));
            //classComboBox.DataSource = Enum.GetValues(typeof(SubclassEnum));

            // Build connection to database
            //SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            this.builder = new SqlConnectionStringBuilder();
            this.builder.DataSource = "PC7381";
            this.builder.InitialCatalog = "RPG Character Generator";
            this.builder.IntegratedSecurity = true;
            Console.WriteLine("Connecting to SQL Server...");

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void submit_Click(object sender, EventArgs e)
        {
            string name;
            RaceEnum race = (RaceEnum)Enum.Parse(typeof(RaceEnum), raceComboBox.SelectedItem.ToString());
            ClassEnum className = (ClassEnum)Enum.Parse(typeof(ClassEnum), classComboBox.SelectedItem.ToString());
            //SubclassEnum className = (SubclassEnum)Enum.Parse(typeof(SubclassEnum), classComboBox.SelectedItem.ToString());
            GenderEnum gender;
            dynamic character;

            if (nameTextBox.Text != "")
                name = nameTextBox.Text;
            else
            {
                MessageBox.Show("Please enter a name!");
                return;
            }

            if (maleRadioButton.Checked)
                gender = GenderEnum.Male;
            else
                gender = GenderEnum.Female;

            switch (className)
            {
                case ClassEnum.Knight:
                    character = new Knight(name, gender, race);
                    break;
                case ClassEnum.Defender:
                    character = new Defender(name, gender, race);
                    break;
                case ClassEnum.Healer:
                    character = new Healer(name, gender, race);
                    break;
                case ClassEnum.BattleMage:
                    character = new BattleMage(name, gender, race);
                    break;
                case ClassEnum.Archer:
                    character = new Archer(name, gender, race);
                    break;
                case ClassEnum.Assassin:
                    character = new Assassin(name, gender, race);
                    break;
                default:
                    character = new Knight(name, gender, race);
                    break;
            }

            /*
            MessageBox.Show(character.ToString());

            string directory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);


            using (TextWriter txt = new StreamWriter($@"{directory}\character.txt"))
            {
                txt.Writ
                e(character.ToString());
            }
            */


            try
            {
                using (SqlConnection connection = new SqlConnection(this.builder.ConnectionString))
                {
                    connection.Open();

                    Console.WriteLine($"Done. \n");


                    string sql = "INSERT INTO Character (Name, Race, Gender, MaxHP, " +
                        "MaxEnergy, ArmorRating, Strength, Intelligence, Agility, ClassId) " +
                        "VALUES (@Name, @Race, @Gender, @MaxHp, @MaxEnergy, @ArmorRating, @Strength, " +
                        "@Intelligence, @Agility, @ClassId)";
                  
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        
                        command.Parameters.AddWithValue("@Name", character.Name);
                        command.Parameters.AddWithValue("@Race", character.Race);
                        command.Parameters.AddWithValue("@Gender", character.Gender);
                        command.Parameters.AddWithValue("@MaxHP", character.HealthPoints);
                        command.Parameters.AddWithValue("@MaxEnergy", character.Energy);
                        
                        /*
                        if (character.ClassName == ClassEnum.Warrior)
                            command.Parameters.AddWithValue("@MaxFury", character.Fury);
                        else
                            command.Parameters.AddWithValue("@MaxFury", 0);

                        if (character.ClassName == ClassEnum.Mage) 
                            command.Parameters.AddWithValue("@MaxMana", character.Mana);
                        else
                            command.Parameters.AddWithValue("@MaxMana", 0);
                        
                        if (character.ClassName == ClassEnum.Thief)
                            command.Parameters.AddWithValue("@MaxEnergy", character.Energy);
                        else
                            command.Parameters.AddWithValue("@MaxEnergy", 0);
                        */

                        command.Parameters.AddWithValue("@ArmorRating", character.ArmorRating);
                        command.Parameters.AddWithValue("@Strength", character.Strength);
                        command.Parameters.AddWithValue("@Intelligence", character.Intelligence);
                        command.Parameters.AddWithValue("@Agility", character.Agility);

                        if (character.ClassName == ClassEnum.Knight)
                            command.Parameters.AddWithValue("@ClassId", 1);
                        else if (character.ClassName == ClassEnum.Defender)
                            command.Parameters.AddWithValue("@ClassId", 2);
                        else if (character.ClassName == ClassEnum.Healer)
                            command.Parameters.AddWithValue("@ClassId", 3);
                        else if (character.ClassName == ClassEnum.BattleMage)
                            command.Parameters.AddWithValue("@ClassId", 4);
                        else if (character.ClassName == ClassEnum.Archer)
                            command.Parameters.AddWithValue("@ClassId", 5);
                        else if (character.ClassName == ClassEnum.Assassin)
                            command.Parameters.AddWithValue("@ClassId", 6);

                        command.ExecuteNonQuery();
                        
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        private void maleRadioButton_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void femaleRadioButton_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void mainTitle_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void nameTextBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
